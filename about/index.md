---
layout: page
title: About
---
## This Site

A forever Work In Progress by me.

Frankenstein mash up build of Mauricio Urraco's [Minimal Jekyll Resume Theme](https://github.com/murraco/jekyll-theme-minimal-resume) & Jekyll theme Poole and Lanyon by [@mdo](https://twitter.com/mdo)

Proudly running Let's Encrypt security certificate using Netlify. 

Hosted on GitHub. 

Privacy policy by [privacypolicygenerator.info](https://privacypolicygenerator.info)


